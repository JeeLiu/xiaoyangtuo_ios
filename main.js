cc.game.onStart = function(){
	/*
    cc.view.setDesignResolutionSize(800, 450, cc.ResolutionPolicy.EXACT_FIT);
	cc.view.resizeWithBrowserSize(true);
    //load resources
    cc.LoaderScene.preload(g_resources, function () {
        cc.director.runScene(new HelloWorldScene());
    }, this);
    //*/
	
	cc.view.adjustViewPort(true);
	
	if (cc.sys.isMobile) {
		cc.view.setDesignResolutionSize(320, 500, cc.ResolutionPolicy.FIXED_WIDTH);
	} else {
		cc.view.setDesignResolutionSize(320, 480, cc.ResolutionPolicy.SHOW_ALL);
	}
	cc.view.resizeWithBrowserSize(true);
	
	cc.LoaderScene.preload(g_resources, function () {
		gameScene = new GameScene();
		cc.director.runScene(gameScene);
	}, this);
};
cc.game.run();