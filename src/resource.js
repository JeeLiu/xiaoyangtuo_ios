var res = {
    HelloWorld_png : "res/HelloWorld.png",
    CloseNormal_png : "res/CloseNormal.png",
    CloseSelected_png : "res/CloseSelected.png",
    	
    	bg : "res/background.jpg",
    	block : "res/block.png",
    	player : "res/player.png",
    	succeed : "res/succeed.png",
    	failed : "res/failed.png",
    	again : "res/replay.png",
    	notify : "res/notify.png",
    	start : "res/title.png",
    	more : "res/more.png",
    	arrow : "res/arrow.png",
    	background_mp3 : "res/bg.mp3",
    		click : "res/click.mp3"
};
/*
var g_resources = [
    //image
    res.HelloWorld_png,
    res.CloseNormal_png,
    res.CloseSelected_png

    //plist

    //fnt

    //tmx

    //bgm

    //effect
];
//*/
var g_resources = []
for ( var i in res ) {
	g_resources.push(res[i])
}
